//
//  Constants.swift
//  CGShop
//
//  Created by mac on 18/07/2018.
//  Copyright © 2018 cg. All rights reserved.
//

struct ENV {
    struct API {
        static let BASE_URL = "http://desafiobrq.herokuapp.com/v1/"
    }
}
