//
//  ServiceError.swift
//  CGShop
//
//  Created by mac on 19/07/2018.
//  Copyright © 2018 cg. All rights reserved.
//

import Foundation

enum ServiceError : Error{
    case wrongJSONParsing
}
